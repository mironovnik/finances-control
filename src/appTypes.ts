export type mapStatePropsType = {
    isAuth: boolean
}

export type mapDispatchPropsType = {
    autoLogin: () => void,
    getAllStatisticFromRemote: (userId: string) => void,
    getAllStatisticFromLocal: () => void,
    getCategoriesFromRemote: (userId: string) => void,
    getCategoriesFromLocal: () => void
}

export type OwnPropsType = {

}

export type AppPropsType = mapStatePropsType & mapDispatchPropsType & OwnPropsType