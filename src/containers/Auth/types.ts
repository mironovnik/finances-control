export type MapDispatchPropsType = {
    setPageName: (pageName: string) => void,
    auth: (email: string, password: string, isLogin: boolean) => void
}

export type MapStatePropsType = {
    loginLoader: boolean,
    registerLoader: boolean
}

export type OwnPropsType = {

}

export type AuthFormControlNamesType = "email" | "password"

export type PropsType = MapStatePropsType & MapDispatchPropsType & OwnPropsType