import React, { useEffect, useState, FormEvent } from 'react'
import classes from './Auth.module.scss'
import { connect } from 'react-redux'
import { AppStateType } from '../../store/reducers/rootReducer'
import { setPageName } from '../../store/actions/layout/layoutAction'
import { PropsType, AuthFormControlNamesType } from './types'
import Button from '../../components/UI/Button/Button'
import Input from '../../components/UI/Input/Input'
import { formControlsType } from '../../components/Income/AddIncomeForm/types'
import Loader from '../../components/UI/Loader/Loader'
import { auth } from '../../store/actions/auth/authAction'

const Auth: React.FC<PropsType> = (props: PropsType) => {

    useEffect(() => {
        props.setPageName(`Личный кабинет`);
    });

    const [formControls, setFromControls] = useState<formControlsType>({
        'email': {
            type: 'text',
            value: '',
            label: 'Электронная почта',
            placeholder: 'Введите email',
            errorMessage: 'Некорректный email',
            touched: false,
            valid: false,
            validationRules: {
                required: true,
                email: true
            }
        },
        'password': {
            type: 'password',
            value: '',
            label: 'Пароль',
            placeholder: 'Введите пароль',
            errorMessage: 'Неверный пароль',
            touched: false,
            valid: false,
            validationRules: {
                required: true,
                minLength: 6
            }
        }
    });

    const [isFormValid, setValidForm] = useState(false);

    const validateAuthFormControl = (value: string, controlName: AuthFormControlNamesType): boolean => {
        let isControlValid = true;
        const validationRules = formControls[controlName].validationRules;
        if (validationRules?.required) {
            isControlValid = value !== "" && isControlValid;
        }
        if (validationRules?.email) {
            const emailRegex = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
            isControlValid = emailRegex.test(value) && isControlValid;
        }
        if (validationRules?.minLength) {
            isControlValid = value.length >= validationRules?.minLength && isControlValid;
        }
        return isControlValid;
    }

    const onChangeInputHandler = (value: string, controlName: AuthFormControlNamesType) => {
        setValidForm(true);
        const formControlsCopy = { ...formControls };
        formControlsCopy[controlName].value = value;
        formControlsCopy[controlName].touched = true;
        formControlsCopy[controlName].valid = validateAuthFormControl(value, controlName)

        for (let control in formControlsCopy) {
            if (!formControlsCopy[control].valid) {
                setValidForm(false);
                break;
            }
        }

        setFromControls(formControlsCopy);
    }

    const onSubmitHandler = (evt: FormEvent) => {
        evt.preventDefault();
    }

    const onLoginHandler = () => {
        props.auth(formControls.email.value, formControls.password.value, true);
        setValidForm(false);
    }

    const onRegisterHandler = () => {
        props.auth(formControls.email.value, formControls.password.value, false);
        setValidForm(false);
    }
    const renderInputs = () => {
        const formControlsNames: Array<AuthFormControlNamesType> = Object.keys(formControls) as Array<AuthFormControlNamesType>;
        return formControlsNames.map((controlName: AuthFormControlNamesType) => {
            const { type, label, value, placeholder, errorMessage, touched, valid, validationRules } = formControls[controlName];
            return <Input
                key={controlName}
                type={type}
                label={label}
                value={value}
                placeholder={placeholder}
                errorMessage={errorMessage}
                touched={touched}
                valid={valid}
                shouldValidate={!!validationRules}
                onChangeHandler={({ target }) => onChangeInputHandler(target.value, controlName)}
            />
        })
    }

    const loaders = [props.loginLoader, props.registerLoader];

    const renderButtons = () => {
        const buttons = [
            { text: `Вход`, onClickHandler: onLoginHandler },
            { text: `Регистрация`, onClickHandler: onRegisterHandler },
        ]
        return buttons.map(({ text, onClickHandler }, index) => {
            return <Button
                key={text}
                disabled={!isFormValid}
                text={text}
                type={isFormValid ? "income" : "disabled"}
                onClickHandler={onClickHandler}
                loader={loaders[index] ? <Loader /> : undefined}
            />
        })
    }


    return (
        <div className={classes[`Auth`]}>
            <form action="" onSubmit={onSubmitHandler}>
                <div className={classes[`Auth__inputs`]}>
                    {renderInputs()}
                </div>
                <div className={classes[`Auth__buttons`]}>
                    {renderButtons()}
                </div>
            </form>
        </div>
    )
}

const mapState = (state: AppStateType) => {
    return {
        registerLoader: state.auth.registerLoader,
        loginLoader: state.auth.loginLoader
    }
}

const mapDispatch = { setPageName, auth }

const connector = connect(mapState, mapDispatch);

export default connector(Auth)
