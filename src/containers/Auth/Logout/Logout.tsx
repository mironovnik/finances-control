import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { authLogout } from '../../../store/actions/auth/authAction'
import { PropsTypes } from './types'
import { Redirect } from 'react-router-dom'

const Logout: React.FC<PropsTypes> = (props: PropsTypes) => {

    useEffect(() => {
        props.authLogout()
    })

    return (
        <Redirect to={"/"} />
    )
}

const mapDispatch = { authLogout }

const connector = connect(null, mapDispatch);

export default connector(Logout)