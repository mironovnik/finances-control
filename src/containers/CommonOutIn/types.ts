import { formRoleType } from '../../components/Income/AddIncomeForm/types'

export type IncomeType = {
    date: string,
    category: string,
    comment?: string,
    amount: number
}

export type IncomesObj = {
    [key: string]: Array<IncomeType>
}

export type MapStatePropsType = {
    displayedDate: {
        monthIndex: number,
        year: number
    },
    displayedStatisticIndex: string,
    monthStatistic: IncomesObj
}

export type MapsDispatchPropsType = {
    getIncomesStatisticByMonth: (monthIndex: number) => void,
    toggleOverlay: (toggleMode: boolean) => void,
    toggleIncomeForm: (toggleMode: boolean, formType: formRoleType) => void,
    setPageName: (pageName: string) => void
}

export type OwnPropsType = {
    pageName: string,
    statisticArrayName: "monthOutcomesStatistic" | "monthIncomesStatistic",
    formRoleType: formRoleType,
}

export type Props = OwnPropsType & MapStatePropsType & MapsDispatchPropsType;