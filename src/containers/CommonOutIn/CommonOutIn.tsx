import React, { useEffect } from 'react'
import classes from './CommonOutIn.module.scss'
import Income from '../../components/Income/Income'
import TotalIncomeInMounth from '../../components/Income/TotalIncomeInMounth/TotalIncomeInMounth'
import { connect } from 'react-redux'
import { Props, OwnPropsType, IncomeType } from './types'
import { getIncomesStatisticByMonth } from '../../store/actions/incomes/IncomesAction'
import { toggleOverlay, toggleIncomeForm, setPageName } from '../../store/actions/layout/layoutAction'
import { AppStateType } from '../../store/reducers/rootReducer'
import Button from '../../components/UI/Button/Button'

const CommonOutIn: React.FC<Props> = (props: Props) => {

    useEffect(() => {
        props.setPageName(props.pageName);
    })

    const getTotalIncomesValue = (statIndex: number): number => {
        if (!props.monthStatistic[statIndex]) return 0;
        return props.monthStatistic[statIndex].reduce((acc, statisticItem: IncomeType) => acc + Number(statisticItem.amount), 0);
    }

    const renderIncomes = (statIndex: number): (JSX.Element[] | null) => {
        if (!props.monthStatistic[statIndex]) return null;
        return props.monthStatistic[statIndex].map((statisticItem: IncomeType, index: number) => {
            const { date, category, comment, amount } = statisticItem;
            return <Income
                key={`${props.statisticArrayName}-${index}`}
                date={date}
                category={category}
                comment={comment}
                amount={amount}
                currency={`RUB`}
            />
        })
    }

    const getMonthWording = (mounthIndex: number): string => {
        const months: string[] = ['Январь', 'Февраль', 'Март', 'Апрель', 'Май', 'Июнь', 'Июль', 'Август', 'Сентябрь', 'Октябрь', 'Ноябрь', 'Декабрь'];
        return months[mounthIndex]
    }


    return (
        <>
            <div className={classes[`CommonOutIn`]}>
                <div className={classes[`CommonOutIn__date`]}>
                    <div
                        className={classes[`CommonOutIn__prevDateItem`]}
                        onClick={() => props.getIncomesStatisticByMonth(props.displayedDate.monthIndex - 1)}
                    />
                    <div className={classes[`CommonOutIn__dateItemValue`]}>
                        {getMonthWording(props.displayedDate.monthIndex) + ' ' + props.displayedDate.year}
                    </div>
                    <div
                        className={classes[`CommonOutIn__nextDateItem`]}
                        onClick={() => { props.getIncomesStatisticByMonth(props.displayedDate.monthIndex + 1) }}
                    />
                </div>
                <div className={classes[`CommonOutIn__detail-info`]}>
                    <TotalIncomeInMounth
                        total={getTotalIncomesValue(+props.displayedStatisticIndex)}
                        currency={`RUB`}
                    />
                    {renderIncomes(+props.displayedStatisticIndex)}
                </div>
            </div>
            <Button
                text={'Добавить'}
                disabled={false}
                onClickHandler={() => {
                    props.toggleOverlay(false)
                    props.toggleIncomeForm(false, props.formRoleType)
                }}
                type={'income'}
            />
        </>
    )
}

const mapState = (state: AppStateType, ownProps: OwnPropsType) => {
    return {
        displayedDate: state.incomes.displayedDate,
        displayedStatisticIndex: state.incomes.displayedStatisticIndex,
        monthStatistic: state.incomes[ownProps.statisticArrayName]
    }
}
const mapDispatch = { getIncomesStatisticByMonth, toggleOverlay, toggleIncomeForm, setPageName}

const connector = connect(mapState, mapDispatch);

export default connector(CommonOutIn);