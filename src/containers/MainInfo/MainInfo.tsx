import React, { useEffect } from 'react'
import classes from './MainInfo.module.scss'
import WalletStatus from '../../components/WalletStatus/WalletStatus'
import { connect } from 'react-redux'
import { setPageName } from '../../store/actions/layout/layoutAction'
import { PropsType } from './types';
import { AppStateType } from '../../store/reducers/rootReducer'
import { IncomesObj } from '../CommonOutIn/types'

const MainInfo: React.FC<PropsType> = (props: PropsType) => {

    useEffect(() => {
        props.setPageName(`Главная`);
    })

    const calculateMonthSumm = (statistic: IncomesObj) => {
        if (!Object.keys(statistic).length) return 0;
        return statistic[(new Date().getMonth() + 1 + '' + new Date().getFullYear())].reduce((acc, stat) => acc + Number(stat.amount), 0)
    }

    return (
        <div className={classes.MainInfo}>
            <div className={classes['MainInfo__wrapper']}>
                <WalletStatus
                    value={calculateMonthSumm(props.monthIncomesStatistic)}
                    currency={`RUB`}
                    income={true}
                >
                </WalletStatus>
                <WalletStatus
                    value={calculateMonthSumm(props.monthOutcomesStatistic)}
                    currency={`RUB`}
                    income={false}
                >
                </WalletStatus>
            </div>
        </div>
    )
}

const mapState = (state: AppStateType) => {
    return {
        monthIncomesStatistic: state.incomes.monthIncomesStatistic,
        monthOutcomesStatistic: state.incomes.monthOutcomesStatistic
    }
}

const mapDispatch = { setPageName }

const connector = connect(mapState, mapDispatch);

export default connector(MainInfo)