import { IncomesObj } from '../CommonOutIn/types'

export type MapStatePropsType = {
    monthIncomesStatistic: IncomesObj,
    monthOutcomesStatistic: IncomesObj
}

export type MapDispatchPropsType = {
    setPageName: (pageName: string) => void
}

export type PropsType = MapStatePropsType & MapDispatchPropsType