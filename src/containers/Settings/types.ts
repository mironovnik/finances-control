export type MapDispatchPropsType = {
    setPageName: (pageName: string) => void
}

export type PropsType = MapDispatchPropsType