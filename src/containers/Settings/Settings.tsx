import React, {useEffect} from 'react'
import classes from './Settings.module.scss'
import {NavLink} from 'react-router-dom'
import { setPageName } from '../../store/actions/layout/layoutAction'
import {connect} from 'react-redux'
import {PropsType} from './types'

const Settings: React.FC<PropsType> = (props: PropsType) => {
    useEffect(()=> {
        props.setPageName(`Настройки`);
    });
    return (
        <div className={classes[`Settings`]}>
            <NavLink to={'/settings/categories'}>Категории</NavLink>
        </div>
    )
}

const mapDispatch = { setPageName }

const connector = connect(null, mapDispatch);

export default connector(Settings)