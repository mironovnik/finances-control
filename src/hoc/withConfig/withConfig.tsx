import React from 'react'
import {withConfigType} from './types'
import {Props} from '../../containers/CommonOutIn/types'

const withConfig = (WrappedComponent: React.ComponentType<Props>, additionalProps: withConfigType) => {
    return ({ ...props }) => <WrappedComponent {...{...props as Props, ...additionalProps}}/>
}

export default withConfig