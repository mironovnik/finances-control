export type withConfigType = {
    pageName: string,
    statisticArrayName: "monthOutcomesStatistic" | "monthIncomesStatistic",
    formRoleType: "income" | "outcome",
}