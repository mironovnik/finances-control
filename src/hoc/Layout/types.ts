import React from 'react';
import { formRoleType } from './../../components/Income/AddIncomeForm/types';

export type MapStatePropsType = {
    menu: boolean,
    overlay: boolean,
    addIncomeForm: boolean,
    pageName: string,
    formRole: formRoleType,
    isAuth: boolean,
    addCategoryForm: boolean
}

export type displayedDateType = {
    displayedDate: {
        monthIndex: number,
        year: number
    }
}

export type OwnPropsType = {
    children: React.ReactNode
}

export type MapDispatchPropsType = {
    toggleOverlay: (toggleMode: boolean) => void,
    toggleMenu: (toggleMode: boolean) => void,
    toggleIncomeForm: (toggleMode: boolean, formType: formRoleType) => void,
    toggleCategoryForm: (formRole: formRoleType) => void
}

export type PropsType = MapStatePropsType & MapDispatchPropsType & OwnPropsType & displayedDateType