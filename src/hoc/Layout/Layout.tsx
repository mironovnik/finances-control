import * as React from 'react'
import { Component } from 'react'
import classes from './Layout.module.scss'
import NavPanel from '../../components/Navigation/NavPanel/NavPanel'
import MainNavigation from '../../components/Navigation/MainNavigation/MainNavigation'
import Overlay from '../../components/UI/Overlay/Overlay'
import { connect } from 'react-redux'
import { AppStateType } from '../../store/reducers/rootReducer'
import { PropsType } from './types'
import { toggleOverlay, toggleMenu, toggleIncomeForm, toggleCategoryForm } from '../../store/actions/layout/layoutAction'
import AddIncomeForm from '../../components/Income/AddIncomeForm/AddIncomeForm'
import AddCategoryForm from '../../components/Categories/AddCategoryForm/AddCategoryForm'

class Layout extends Component<PropsType> {

    toggleMenuHandler = () => {
        this.props.toggleOverlay(this.props.overlay);
        this.props.toggleMenu(this.props.menu);
    }

    render() {
        return (
            <div className={classes.Layout}>
                {this.props.addCategoryForm
                    ? <AddCategoryForm 
                        formRoleType={this.props.formRole}
                        onCrossClickHandler={()=>{
                            this.props.toggleOverlay(true);
                            this.props.toggleCategoryForm(this.props.formRole);
                        }}
                    />
                    : null}
                {this.props.addIncomeForm
                    ? <AddIncomeForm
                        formRoleType={this.props.formRole}
                        onCrossClickHadler={() => {
                            this.props.toggleIncomeForm(true, this.props.formRole);
                            this.props.toggleOverlay(true);
                        }}
                        displayedDate={this.props.displayedDate}
                    />
                    : null}

                {
                    this.props.overlay
                        ? <Overlay
                            closeMenu={() => {
                                this.props.toggleOverlay(this.props.overlay)
                                if (this.props.menu) this.props.toggleMenu(true);
                                if (this.props.addIncomeForm) this.props.toggleIncomeForm(true, this.props.formRole);
                            }}
                        />
                        : null
                }
                <MainNavigation
                    menuStatus={this.props.menu}
                    closeMenu={this.toggleMenuHandler}
                    isAuth={this.props.isAuth}
                />
                <NavPanel
                    toggleMenu={this.toggleMenuHandler}
                    pageName={this.props.pageName}
                />
                <main>
                    {this.props.children}
                </main>
            </div>
        )
    }
}

const mapState = (state: AppStateType) => {
    return {
        overlay: state.layout.overlay,
        menu: state.layout.menu,
        addIncomeForm: state.layout.addIncomeForm,
        pageName: state.layout.pageName,
        displayedDate: state.incomes.displayedDate,
        formRole: state.layout.formRole,
        isAuth: !!state.auth.token,
        addCategoryForm: state.layout.addCategoryForm
    }
}

const mapDispatch = { toggleOverlay, toggleMenu, toggleIncomeForm, toggleCategoryForm }

const connector = connect(mapState, mapDispatch);

export default connector(Layout);