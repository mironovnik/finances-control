import React, { useEffect } from 'react'
import MainInfo from './containers/MainInfo/MainInfo'
import Layout from './hoc/Layout/Layout'
import Settings from './containers/Settings/Settings'
import { Route, Switch, withRouter, Redirect } from 'react-router-dom'
import Categories from './components/Categories/Categories'
import CommonOutIn from './containers/CommonOutIn/CommonOutIn'
import Logout from './containers/Auth/Logout/Logout'
import withConfig from './hoc/withConfig/withConfig'
import { withConfigType } from './hoc/withConfig/types'
import Auth from './containers/Auth/Auth'
import { connect } from 'react-redux'
import { AppStateType } from './store/reducers/rootReducer'
import { AppPropsType } from './appTypes'
import { autoLogin } from './store/actions/auth/authAction'
import { getAllStatisticFromRemote, getAllStatisticFromLocal } from './store/actions/incomes/IncomesAction'
import { getCategoriesFromRemote, getCategoriesFromLocal } from './store/actions/categories/categoriesAction'

const App: React.FC<AppPropsType> = (props: AppPropsType) => {
  //test userid
  // const userId = `dUZy0gjGPMUGOBAIRNdWihDn9mC2`; 
  useEffect(() => {
    props.autoLogin();
    const userId = localStorage.getItem(`userId`);
    if (userId) {
      props.getAllStatisticFromRemote(userId);
      props.getCategoriesFromRemote(userId);
    } else {
      props.getAllStatisticFromLocal();
      props.getCategoriesFromLocal();
    }
  })

  const outcomesConfig: withConfigType = {
    pageName: 'Расходы',
    statisticArrayName: "monthOutcomesStatistic",
    formRoleType: "outcome"
  }

  const incomesConfig: withConfigType = {
    pageName: 'Доходы',
    statisticArrayName: "monthIncomesStatistic",
    formRoleType: "income"
  }

  let routes = (
    <Switch>
      <Route path="/incomes" component={withConfig(CommonOutIn, incomesConfig)}></Route>
      <Route path="/outcomes" component={withConfig(CommonOutIn, outcomesConfig)}></Route>
      <Route path="/settings/categories" component={Categories}></Route>
      <Route path="/settings" component={Settings}></Route>
      <Route path="/auth" component={Auth}></Route>
      <Route path="/" exact component={MainInfo} ></Route>
      <Redirect to="/" />
    </Switch>
  );

  if (props.isAuth) {
    routes = (
      <Switch>
        <Route path="/incomes" component={withConfig(CommonOutIn, incomesConfig)}></Route>
        <Route path="/outcomes" component={withConfig(CommonOutIn, outcomesConfig)}></Route>
        <Route path="/settings/categories" component={Categories}></Route>
        <Route path="/settings" component={Settings}></Route>
        <Route path="/logout" component={Logout}></Route>
        <Route path="/" exact component={MainInfo} ></Route>
        <Redirect to="/" />
      </Switch>)
  }

  return (
    <Layout>
      {routes}
    </Layout>
  )
}

const mapState = (state: AppStateType) => {
  return {
    isAuth: !!state.auth.token
  }
}

const mapDispatch = {
  autoLogin,
  getAllStatisticFromRemote,
  getAllStatisticFromLocal,
  getCategoriesFromRemote,
  getCategoriesFromLocal,
}

const connector = connect(mapState, mapDispatch);

export default withRouter(connector(App));