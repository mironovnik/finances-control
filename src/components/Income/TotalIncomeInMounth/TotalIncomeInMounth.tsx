import React from 'react'
import classes from './TotalIncomeInMounth.module.scss'

interface Props {
    total: number,
    currency: string
}

const TotalIncomeInMounth: React.FC<Props> = (props: Props) => {
    return (
        <div className={classes['TotalIncomeInMounth']}>
            <div>Итого</div>
            <div className={classes['TotalIncomeInMounth__value']}>{props.total} {props.currency}</div>
        </div>
    )
}

export default TotalIncomeInMounth;