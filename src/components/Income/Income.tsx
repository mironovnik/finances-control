import React from 'react'
import classes from './Income.module.scss'
import { PropsType } from './types'

const Income: React.FC<PropsType> = (props: PropsType) => {
    return (
        <div className={classes['Income']}>
            <div>{props.date}</div>
            <div className={classes['Income__category']}>
                <div>{props.category}</div>
                <div className={classes['Income__comment']}>{props.comment}</div>
            </div>
            <div className={classes['Income__value']}>{`${props.amount} ${props.currency}`} </div>
        </div>
    )
}

export default Income;