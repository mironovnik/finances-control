import React, { useState, FormEvent, ChangeEvent } from 'react'
import classes from './AddIncomeForm.module.scss'
import Cross from '../../UI/Cross/Cross';
import Input from '../../UI/Input/Input';
import Select from '../../UI/Select/Select';
import Button from '../../UI/Button/Button';
import { PropsType, formDataType, inputNamesType, formControlsType, formControlType, validationRulesType, OwnPropsType } from '../../Income/AddIncomeForm/types';
import { optionType } from '../../UI/Select/types';
import { AppStateType } from '../../../store/reducers/rootReducer';
import { connect } from 'react-redux';
import { createIncome } from '../../../store/actions/incomes/IncomesAction'

const setDefautChoosenCategory = (options: Array<optionType>): string => {
    const defaultValue = options.filter(option => option.defaultValue);
    return defaultValue.length ? defaultValue[0].text : options[0].text
}

const getDateIndex = (date: string): string => {
    let sliceIndex = -7;
    let spliceIndex = 2;
    if (date.slice(3, 4) === '0') {
        sliceIndex = -6;
        spliceIndex = 1;
    }
    const splittedDate = date.slice(sliceIndex).split("");
    splittedDate.splice(spliceIndex, 1);
    return splittedDate.join("");
}

const getFullDate = () => {
    const date = new Date();
    return date.getDate() + '.' + (date.getMonth() < 10 ? `0${date.getMonth() + 1}` : '' + date.getMonth() + 1) + '.' + new Date().getFullYear()
}

const getDefaultDate = (monthIndex: number, year: number) => '01.' + (monthIndex < 10 ? `0${monthIndex + 1}.` : monthIndex + 1 + '.') + year

const dateRegExp = /^(3[01]|[12][0-9]|0?[1-9])([.-])(1[0-2]|0?[1-9])([.-])(?:[0-9]{2}[0-9]{2})$/;

const AddIncomeForm: React.FC<PropsType> = (props: PropsType) => {

    const options = props.formRoleType === `income` ? props.categories.incomes : props.categories.outcomes

    const [isFormValid, toggleValidStatus] = useState(false);

    const [formControls, setFromControls] = useState<formControlsType>({
        'amount': {
            type: "text",
            label: 'Сумма',
            value: '',
            placeholder: "Введите сумму",
            errorMessage: 'Введите корректное значение',
            touched: false,
            valid: false,
            validationRules: {
                required: true,
                positiveNumber: true
            }
        },
        'date': {
            type: "text",
            value: props.displayedDate.monthIndex === new Date().getMonth() ? getFullDate() : getDefaultDate(props.displayedDate.monthIndex, props.displayedDate.year),
            label: 'Дата',
            placeholder: 'дд.мм.гггг',
            errorMessage: 'Неверный формат даты',
            touched: false,
            valid: true,
            maxLength: 10,
            validationRules: {
                required: true,
                date: true,
            }
        },
        'comment': {
            type: "text",
            label: 'Комментарий',
            touched: false,
            valid: true,
            value: ''
        }
    });

    const [selectedCategory, setCategory] = useState(setDefautChoosenCategory(options));

    const onSubmitFormHandler = (evt: FormEvent) => {
        evt.preventDefault();
        const addIncomeformData: formDataType = {
            category: selectedCategory,
            amount: formControls.amount.value as unknown as number,
            date: formControls.date.value,
            comment: formControls.comment.value
        }
        const monthStatistic = { ...props.monthStatistic };
        const dateIndex = getDateIndex(addIncomeformData.date);
        if (monthStatistic[dateIndex]) {
            monthStatistic[dateIndex].push(addIncomeformData);
        } else {
            monthStatistic[dateIndex] = [addIncomeformData]
        }
        props.createIncome(monthStatistic, props.formRoleType);
        props.onCrossClickHadler();
    }

    const onChangeSelectHandler = ({ target }: ChangeEvent<HTMLSelectElement>) => setCategory(target.value)

    const isControlValid = (value: string, { required, positiveNumber, date }: validationRulesType): boolean => {
        let controlValid = true;
        if (required) {
            controlValid = value !== "" && controlValid;
        }
        if (positiveNumber) {
            controlValid = !!parseFloat(value) && value as unknown as number > 0 && controlValid;
        }
        if (date) {
            controlValid = dateRegExp.test(value) && controlValid;
        }
        return controlValid
    }

    const onInputChangeHandler = (value: string, controlName: inputNamesType) => {
        const formControlsCopy = { ...formControls };
        const targetedFormControl = formControlsCopy[controlName];
        targetedFormControl.value = value;
        targetedFormControl.touched = true;
        if (targetedFormControl.validationRules) {
            targetedFormControl.valid = isControlValid(value, targetedFormControl.validationRules);
        }

        toggleValidStatus(true);

        for (let control in formControlsCopy) {
            if (!formControlsCopy[control].valid) {
                toggleValidStatus(false);
                break;
            }
        }

        setFromControls(formControlsCopy);
    }

    const select = (
        <Select
            label={"Категория"}
            options={options}
            onChangeHandler={onChangeSelectHandler}
        />
    );

    const renderInputs = () => {
        const formControlKeys = Object.keys(formControls) as inputNamesType[];
        return formControlKeys.map((controlName: inputNamesType, index: number) => {
            const formControl: formControlType = formControls[controlName];
            const { type, label, value, touched, valid, errorMessage, validationRules, placeholder, maxLength } = formControl;
            return <Input
                key={controlName + index}
                type={type}
                label={label}
                value={value}
                placeholder={placeholder}
                touched={touched}
                errorMessage={errorMessage}
                valid={valid}
                shouldValidate={!!validationRules}
                maxLength={maxLength}
                onChangeHandler={({ target }: ChangeEvent<HTMLInputElement>) => { onInputChangeHandler(target.value, controlName) }}
            />
        })
    }

    return (
        <form className={classes[`AddIncomeForm`]} onSubmit={onSubmitFormHandler}>
            <Cross
                onClickHandler={props.onCrossClickHadler}
            />
            {select}
            {renderInputs()}
            <Button
                type={isFormValid ? "income" : "disabled"}
                text="Сохранить"
                disabled={!isFormValid}
                onClickHandler={() => { }}
            />
        </form>
    )
}

const mapState = (state: AppStateType, ownProps: OwnPropsType) => {
    const monthStatistic = ownProps.formRoleType === `income` ? `monthIncomesStatistic` : `monthOutcomesStatistic`;
    return {
        monthStatistic: state.incomes[monthStatistic],
        categories: state.categories
    }
}

const mapDispatch = { createIncome }

const connector = connect(mapState, mapDispatch);

export default connector(AddIncomeForm)
