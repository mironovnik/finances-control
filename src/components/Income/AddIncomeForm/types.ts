import { categoriesType } from './../../Categories/types'
import { IncomesObj } from "../../../containers/CommonOutIn/types"

export type MapStatePropsType = {
    monthStatistic: IncomesObj,
    categories: categoriesType
}

export type MapDispatchPropsType = {
    createIncome: (incomeConfig: IncomesObj, formType: formRoleType) => void
}

export type formRoleType = "income" | "outcome"

export type OwnPropsType = {
    onCrossClickHadler: () => void,
    displayedDate: {
        monthIndex: number,
        year: number
    },
    formRoleType: formRoleType
}

export type PropsType = MapStatePropsType & MapDispatchPropsType & OwnPropsType

export type inputNamesType = "amount" | "date" | "comment"

export type formDataType = {
    category: string,
    amount: number,
    date: string,
    comment?: string
}

export type validationRulesType = {
    required: boolean,
    positiveNumber?: boolean,
    date?: boolean,
    email?: boolean,
    minLength?: number
}

export type formControlType = {
    type: string,
    value: string,
    label: string,
    placeholder?: string,
    errorMessage?: string,
    touched: boolean,
    valid: boolean,
    validationRules?: validationRulesType,
    maxLength?: number,
    minLength?: number
}

export type formControlsType = {
    [key: string]: formControlType
}