export type PropsType = {
    date: string,
    category: string,
    comment?: string,
    amount: number,
    currency: string
}