import React from 'react'
import classes from './WalletStatus.module.scss';
import { NavLink } from 'react-router-dom';

interface Props {
    value: number,
    currency: string,
    income: boolean
}

const WalletStatus: React.FC<Props> = (props: Props) => {
    const cls: string[] = [classes.WalletStatus];
    const clsName: string[] = [classes[`WalletStatus__value`]];

    if (props.income) {
        cls.push(classes[`WalletStatus__income-icon`]);
        clsName.push(classes[`WalletStatus__value--income`]);
    } else {
        cls.push(classes[`WalletStatus__outcome-icon`]);
        clsName.push(classes[`WalletStatus__value--outcome`]);
    }

    const linkConfig = (<NavLink to={props.income ? "/incomes" : "/outcomes"} exact>{props.income ? `Доход за текущий месяц` : `Расход за текущий месяц`}</NavLink>)

    return (
        <div className={cls.join(" ")}>
            <div className={classes[`WalletStatus__name`]}>
                {linkConfig}
            </div>
            <div className={clsName.join(" ")}>
                {`${props.income ? `+` : `-`}${props.value} ${props.currency}`}
            </div>
        </div>
    )
}

export default WalletStatus;