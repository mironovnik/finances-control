import React, { useEffect, useState } from 'react'
import classes from './Categories.module.scss'
import CategoriesList from './CategoriesList/CategoriesList'
import {connect} from 'react-redux'
import { AppStateType } from '../../store/reducers/rootReducer'
import {PropsType} from './types'
import {setPageName} from '../../store/actions/layout/layoutAction'

const Categories: React.FC<PropsType> = (props: PropsType) => {
    useEffect(()=> {
        props.setPageName(`Категории`);
    });

    const [IncomeDropDown, setIncomeDropDown] = useState(false);
    const [OutcomeDropDown, setOutcomeDropDown] = useState(false);

    const incomeCls = [
        classes[`Categories__item`], 
        classes[`Categories__item--income`],
        IncomeDropDown ? classes[`Categories__item--opened`] : null
    ];
    const outcomeCls = [
        classes[`Categories__item`], 
        classes[`Categories__item--outcome`],
        OutcomeDropDown ? classes[`Categories__item--opened`] : null
    ];

    return (
        <div className={classes[`Categories`]}>
            <div
                className={incomeCls.join(" ")}
                onClick={() => setIncomeDropDown(!IncomeDropDown)}
            >Категории доходов</div>
            <CategoriesList
                isOpen={IncomeDropDown}
                categories={props.categories}
                formRole={`income`}
            />
            <div
                className={outcomeCls.join(" ")}
                onClick={() => setOutcomeDropDown(!OutcomeDropDown)}
            >Категории расходов</div>
            <CategoriesList
                isOpen={OutcomeDropDown}
                categories={props.categories}
                formRole={`outcome`}
            />
        </div>
    )
}

const mapProps = (state: AppStateType) =>{
    return {
        categories: state.categories
    }
}

const mapDispatch = { setPageName }

const connector = connect(mapProps, mapDispatch);

export default connector(Categories);