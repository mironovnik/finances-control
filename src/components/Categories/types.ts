export type optionType = {
    text: string,
    defaultValue: boolean,
    value: string
}

export type keyType = "incomes" | "outcomes"

export type categoriesType = {
    [key in keyType]: Array<optionType>
}

export type MapStatePropsType = {
    categories: categoriesType
}

export type MapDispatchPropsType = {
    setPageName: (pageName: string) => void
}

export type OwnPropsType = categoriesType

export type PropsType = OwnPropsType & MapStatePropsType & MapDispatchPropsType