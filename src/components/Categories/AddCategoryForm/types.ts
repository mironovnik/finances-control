import { formRoleType } from './../../Income/AddIncomeForm/types';

export type MapDispatchPropsType = {
    addCategory: (categoryName: string, formType: formRoleType) => void,
    toggleCategoryForm: (formRole: formRoleType) => void,
    toggleOverlay: (toggleMode: boolean) => void
}

export type OwnPropsType = {
    formRoleType: formRoleType,
    onCrossClickHandler: () => void,
}

export type PropsType = OwnPropsType & MapDispatchPropsType