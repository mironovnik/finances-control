import React, { useState, ChangeEvent, FormEvent } from 'react'
import { PropsType } from './types'
import classes from './AddCategoryForm.module.scss'
import Cross from '../../UI/Cross/Cross'
import Input from '../../UI/Input/Input'
import Button from '../../UI/Button/Button'
import { connect } from 'react-redux'
import { addCategory } from '../../../store/actions/categories/categoriesAction'
import { toggleOverlay, toggleCategoryForm } from '../../../store/actions/layout/layoutAction'

const AddCategoryForm: React.FC<PropsType> = (props: PropsType) => {

    const [inputValue, setInputValue] = useState("");

    const onSubmitHandler = (evt: FormEvent) => {
        evt.preventDefault();
        props.addCategory(inputValue, props.formRoleType);
        props.toggleCategoryForm(`income`);
        props.toggleOverlay(true);
    }

    const header = props.formRoleType === `income` ? `дохода` : `расхода`;

    return (
        <div className={classes[`AddCategoryForm`]}>
            <Cross
                onClickHandler={() => {
                    props.onCrossClickHandler();
                }}
            />
            <h1>Добавление категории {header}</h1>
            <form action="" onSubmit={onSubmitHandler}>
                <Input
                    type={`text`}
                    label={`Введите название категории`}
                    value={inputValue}
                    touched={false}
                    valid={true}
                    shouldValidate={false}
                    onChangeHandler={({ target }: ChangeEvent<HTMLInputElement>) => {
                        setInputValue(target.value)
                    }}
                />
                <Button
                    type={`income`}
                    disabled={false}
                    text={`Добавить`}
                    onClickHandler={() => { }}
                />
            </form>
        </div>
    )
}

const mapDispatch = { addCategory, toggleOverlay, toggleCategoryForm }

const connector = connect(null, mapDispatch);

export default connector(AddCategoryForm);