import React from 'react'
import classes from './CategoriesList.module.scss'
import Button from '../../UI/Button/Button'
import { PropsType, CategoriesType } from './types'
import { optionType } from '../types'
import { connect } from 'react-redux'
import { toggleCategoryForm } from '../../../store/actions/layout/layoutAction'
import { toggleOverlay } from '../../../store/actions/layout/layoutAction'

const CategoriesList: React.FC<PropsType> = (props: PropsType) => {
    const cls = [classes[`Categories-list-container`], props.isOpen ? classes[`Categories-list-container--opened`] : null]
    const key: "incomes" | "outcomes" = props.formRole === `income` ? "incomes" : "outcomes";
    const renderCategories = (categories: CategoriesType) => categories[key].map(({ text }: optionType, index: number) => {
        return (
            <li
                key={`${text}-${index}`}
                className={classes[`CategoriesList__item`]}>
                {text}
            </li>
        )
    })

    return (
        <div className={cls.join(" ")}>
            <ul className={classes[`CategoriesList`]}>
                {renderCategories(props.categories)}
            </ul>
            <Button
                type={`income`}
                text={`Добавить категорию`}
                disabled={false}
                onClickHandler={() => {
                    props.toggleCategoryForm(props.formRole);
                    props.toggleOverlay(false);
                }}
            />
        </div>

    )
}

const mapDispatch = { toggleCategoryForm, toggleOverlay }

const connector = connect(null, mapDispatch);

export default connector(CategoriesList);