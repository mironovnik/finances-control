import { formRoleType } from './../../Income/AddIncomeForm/types';
import { optionType } from '../types'

export type MapDispatchPropsType = {
    toggleCategoryForm: (formRole: formRoleType) => void,
    toggleOverlay: (toggleMode: boolean) => void
}

export type CategoriesType = {
    [key: string]: Array<optionType>
}

export type OwnProps = {
    isOpen: boolean,
    categories: CategoriesType,
    formRole: formRoleType
}

export type PropsType = MapDispatchPropsType & OwnProps