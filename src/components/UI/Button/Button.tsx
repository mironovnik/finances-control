import React from 'react'
import classes from './Button.module.scss'
import { ButtonProps } from './types'

const Button: React.FC<ButtonProps> = (props: ButtonProps) => {
    const cls = [classes[`Button`], (props.type ? classes[props.type] : '')];
    return (
        <div className={cls.join(" ")}>
            <button onClick={props.onClickHandler} disabled={props.disabled}>{!props.loader ? props.text : ""}</button>
            {props.loader}
        </div>
    )
}

export default Button;