import * as React from 'react';

export type ButtonProps = {
    type?: string,
    disabled: boolean,
    text: string,
    onClickHandler: () => void,
    loader?: JSX.Element
}