import React from 'react'
import classes from './Loader.module.scss'

const Loader: React.FC = () => {
    return (
        <div className={classes[`Loader`]}>
            <div/>
            <div/>
            <div/>
            <div/>
        </div>
    )
}

export default Loader