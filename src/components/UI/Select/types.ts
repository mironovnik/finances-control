import { ChangeEvent } from "react"

export type optionType = {
    text: string,
    defaultValue: boolean,
    value: string
}

export type SelectPropsType = {
    label: string,
    options: Array<optionType>,
    onChangeHandler: (evt: ChangeEvent<HTMLSelectElement>) => void
} 