import React from 'react'
import classes from './Select.module.scss'
import { SelectPropsType } from './types'

const Select: React.FC<SelectPropsType> = (props: SelectPropsType) => {
    const htmlFor = `select` + Math.random();
    const defaultValueFiltered = props.options.filter((option) => option.defaultValue);
    const defaultValue = defaultValueFiltered.length ? defaultValueFiltered[0].text : undefined
    return (
        <div className={classes['Select']}>
            <label htmlFor={htmlFor}>{props.label}</label>
            <select name="income-select" id={htmlFor} onChange={props.onChangeHandler} defaultValue={defaultValue}>
                {
                    props.options.map(({ text, value }, index) => {
                        return (<option value={value} key={`income-form-option-${index + 1}`}>{text}</option>)
                    })
                }
            </select>
        </div>
    )
}

export default Select;