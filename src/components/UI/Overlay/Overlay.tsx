import React from 'react'
import classes from './Overlay.module.scss'

interface Props {
    closeMenu: () => void
}

const Overlay: React.FC<Props> = (props: Props) => {
    return (
        <div
            className={classes.Overlay}
            onClick={props.closeMenu}
        ></div>
    )
}

export default Overlay;