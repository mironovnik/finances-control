import React from 'react'
import classes from './Cross.module.scss'
import { PropsType } from './types';

const Cross: React.FC<PropsType> = (props: PropsType) => {
    return (
        <div className={classes['Cross']} onClick={props.onClickHandler}>
            <div className={classes['Cross-right']}></div>
            <div className={classes['Cross-left']}></div>
        </div>
    )
}

export default Cross;