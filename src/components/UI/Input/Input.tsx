import React from 'react'
import classes from './Input.module.scss'
import { InputPropsType } from './types'

const isInputInvalid = ({ valid, touched, shouldValidate }: InputPropsType) => !valid && shouldValidate && touched

const Input: React.FC<InputPropsType> = (props: InputPropsType) => {
    const htmlFor = props.type + Math.random();
    const cls = [classes[`Input`]];
    if (isInputInvalid(props)) {
        cls.push(classes[`Input__error`])
    }
    return (
        <div className={cls.join(" ")}>
            <label htmlFor={htmlFor}>{props.label}</label>
            <input maxLength={props.maxLength} type={props.type} id={htmlFor} value={props.value} placeholder={props.placeholder} onChange={props.onChangeHandler} />
            {
                isInputInvalid(props) ? <span className={classes[`Input__error-message`]}>{props.errorMessage}</span> : null
            }

        </div>
    )
};

export default Input;