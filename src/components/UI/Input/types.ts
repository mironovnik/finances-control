import { ChangeEvent } from 'react';

export type validationRuleAmountType = {
    required: boolean,
    positiveNumber: boolean
}

export type validationRuleDateType = {
    required: boolean,
    date: boolean
}

export type InputPropsType = {
    type: string,
    value?: string,
    label: string,
    placeholder?: string,
    errorMessage?: string,
    touched: boolean,
    valid: boolean,
    maxLength?: number,
    onChangeHandler: (evt: ChangeEvent<HTMLInputElement>) => void
} & {
    shouldValidate: boolean
}