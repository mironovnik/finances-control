import React from 'react'
import classes from './BurgerMenu.module.scss';

interface Props {
    toggleMenu: () => void
}

const BurgerMenu: React.FC<Props> = (props: Props) => {
    return (
        <span
            className={classes.BurgerMenu}
            onClick={props.toggleMenu}
        >
        </span>
    )
}

export default BurgerMenu;