import React from 'react'
import classes from './NavPanel.module.scss'
import BurgerMenu from './BurgerMenu/BurgerMenu';
import FastLinks from './FastLinks/FastLinks';
import {PropsType} from './types';

const NavPanel: React.FC<PropsType> = (props: PropsType): JSX.Element => {
    return (
        <header className={classes[`NavPanel`]}>
            <BurgerMenu
                toggleMenu={props.toggleMenu}
            >
            </BurgerMenu>
    <div className={classes[`NavPanel__pageName`]}>{props.pageName}</div>
            <FastLinks />
        </header>
    )
}

export default NavPanel