import React from 'react'
import classes from './FastLinks.module.scss'
import { NavLink } from 'react-router-dom'

const FastLinks: React.FC = (): JSX.Element => {
    return (
        <ul className={classes.FastLinks}>
            <li >
                <NavLink
                    className={`${classes[`FastLinks__link`]} ${classes[`FastLinks__link--incomes`]}`}
                    to="/incomes"
                />
            </li>
            <li>
                <NavLink
                    className={`${classes[`FastLinks__link`]} ${classes[`FastLinks__link--outcomes`]}`}
                    to="/outcomes"
                />
            </li>
        </ul>
    )
}

export default FastLinks;
