export type PropsType = {
    toggleMenu: () => void,
    pageName: string
}