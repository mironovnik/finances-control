export type mapStatePropsType = {

}

export type mapDispatchPropsType = {

}

export type OwnPropsType = {
    menuStatus: boolean,
    closeMenu: () => void,
    isAuth: boolean
}

export type PropsType = mapStatePropsType & mapDispatchPropsType & OwnPropsType

export type mainNavLinkType = {
    to: string,
    name: string,
    exact: boolean,
    icon: string
}