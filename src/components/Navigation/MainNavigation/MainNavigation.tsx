import React from 'react'
import classes from './MainNavigation.module.scss'
import { NavLink } from 'react-router-dom'
import { PropsType, mainNavLinkType } from './types'

const MainNavigation: React.FC<PropsType> = (props: PropsType) => {

    const renderLinks = (links: mainNavLinkType[]): JSX.Element[] => {
        return links.map(({ to, name, exact, icon }, index: number): JSX.Element => {
            const linkClasses = [classes[`MainNavigation__link`], classes[`MainNavigation__link--${icon}`]];
            return (
                <li
                    className={linkClasses.join(" ")}
                    key={`menu-link-${index}`}
                    onClick={props.closeMenu}
                >
                    <NavLink to={to} exact={exact}>{name}</NavLink>
                </li>
            )
        });
    }


    let links: mainNavLinkType[] = [
        { to: '/', name: 'Главная', exact: true, icon: `home` },
        { to: '/incomes', name: 'Доходы', exact: false, icon: `incomes` },
        { to: '/outcomes', name: 'Расходы', exact: false, icon: `outcomes` },
        { to: '/settings', name: 'Настройки', exact: true, icon: `settings` },
    ];

    if (props.isAuth) {
        links.push({ to: '/logout', name: 'Выйти', exact: true, icon: `logout` })
    } else {
        links.push({ to: '/auth', name: 'Личный кабинет', exact: true, icon: `person` })
    }

    const cls: string[] = [classes.MainNavigation];

    if (!props.menuStatus) {
        cls.push(classes[`MainNavigation--close`])
    }
    return (
        <nav className={cls.join(" ")}>
            <ul>
                {renderLinks(links)}
            </ul>
        </nav>
    )
}

export default MainNavigation;