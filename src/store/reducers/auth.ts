import { authActionTypes, AUTH_SUCCESS, AUTH_START, AUTH_FAIL, AUTH_LOGOUT } from './../actions/auth/authActionTypes'

export type intialStateType = {
    token: string | null,
    registerLoader: boolean,
    loginLoader: boolean
}

const intialState: intialStateType = {
    token: null,
    registerLoader: false,
    loginLoader: false,
}

export default function authReducer(state = intialState, action: authActionTypes) {
    switch (action.type) {
        case AUTH_SUCCESS:
            return {
                ...state,
                token: action.token,
                [action.loader]: false
            }
        case AUTH_START:
            return {
                ...state,
                [action.loader]: true
            }
        case AUTH_FAIL:
            return {
                ...state,
                [action.loader]: false
            }
        case AUTH_LOGOUT:
            return {
                ...state,
                token: null
            }
        default:
            return state
    }
}