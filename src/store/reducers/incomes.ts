import { GET_LOCAL_STATISTIC } from '../actions/incomes/incomesActionTypes'
import { SHOW_MONTH_STAT, CREATE_INCOME, FETCH_STATISTIC_SUCCESS, incomesActionType, IncomesObj } from "../actions/incomes/incomesActionTypes"

export type intialStateType = {
    displayedDate: {
        monthIndex: number,
        year: number
    },
    displayedStatisticIndex: string,
    monthIncomesStatistic: IncomesObj,
    monthOutcomesStatistic: IncomesObj
}

const initialState: intialStateType = {
    displayedDate: {
        monthIndex: +(new Date().getMonth()),
        year: new Date().getFullYear()
    },
    displayedStatisticIndex: new Date().getMonth() + 1 + '' + new Date().getFullYear(),
    monthIncomesStatistic: {},
    monthOutcomesStatistic: {},
}

export default function incomesReducer(state = initialState, action: incomesActionType) {
    switch (action.type) {
        case SHOW_MONTH_STAT: {
            return {
                ...state,
                displayedDate: action.payload.displayedDate,
                displayedStatisticIndex: action.payload.displayedStatisticIndex
            }
        }
        case CREATE_INCOME: {
            return {
                ...state,
                [action.typeOfAdd]: action.payload
            }
        }
        case FETCH_STATISTIC_SUCCESS: {
            return {
                ...state,
                monthIncomesStatistic: action.statistic.monthIncomesStatistic ? action.statistic.monthIncomesStatistic : {},
                monthOutcomesStatistic: action.statistic.monthOutcomesStatistic ? action.statistic.monthOutcomesStatistic : {}
            }
        }
        case GET_LOCAL_STATISTIC: {
            return {
                ...state,
                monthIncomesStatistic: localStorage.getItem(`income_statictic`) ? JSON.parse(localStorage.getItem(`income_statictic`) as string) : {},
                monthOutcomesStatistic: localStorage.getItem(`outcome_statictic`) ? JSON.parse(localStorage.getItem(`outcome_statictic`) as string) : {},
            }
        }
        default:
            return state
    }
}