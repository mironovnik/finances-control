import { ADD_CATEGORY, categoriesActionType } from '../actions/categories/categoriesActionTypes'

export type optionType = {
    text: string,
    defaultValue: boolean,
    value: string
}

export type keyType = "incomes" | "outcomes"

export type initialStateType = {
    [key in keyType]: Array<optionType>
}

const intialState: initialStateType = {
    'incomes': [
        { text: `Работа`, defaultValue: false, value: `Работа` },
        { text: `Инвестиции`, defaultValue: true, value: `Инвестиции` },
        { text: `Подарки`, defaultValue: false, value: `Подарки` },
    ],
    'outcomes': [
        { text: `Еда`, defaultValue: false, value: `Еда` },
        { text: `Интернет`, defaultValue: true, value: `Интернет` },
        { text: `Транспорт`, defaultValue: false, value: `Транспорт` },
    ]
}

export default function categoriesReducer(state = intialState, action: categoriesActionType) {
    switch (action.type) {
        case ADD_CATEGORY:
            return {
                ...state,
                ...action.categories
            }
        default:
            return state
    }
} 