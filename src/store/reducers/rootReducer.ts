import { combineReducers } from "redux"
import incomesReducer from './incomes'
import layoutReducer from './layout'
import categoriesReducer from './categories'
import authReducer from './auth'

const rootReducer = combineReducers({
    incomes: incomesReducer,
    layout: layoutReducer,
    categories: categoriesReducer,
    auth: authReducer
});

export default rootReducer

export type AppStateType = ReturnType<typeof rootReducer>