import { TOGGLE_OVERLAY, TOGGLE_MENU, TOGGLE_FORM, SET_PAGE_NAME, TOGGLE_CATEGORY_FORM, layoutActionType } from '../actions/layout/layoutActionTypes'

export type initialStateType = {
    menu: boolean,
    overlay: boolean,
    addIncomeForm: boolean,
    pageName: string,
    formRole: `income` | `outcome`,
    addCategoryForm: boolean
}

const initialState: initialStateType = {
    menu: false,
    overlay: false,
    addIncomeForm: false,
    pageName: `Главная`,
    formRole: `income`,
    addCategoryForm: false
}

export default function layoutReducer(state = initialState, action: layoutActionType) {
    switch (action.type) {
        case TOGGLE_OVERLAY: {
            return {
                ...state,
                overlay: !action.payload
            }
        }
        case TOGGLE_MENU: {
            return {
                ...state,
                menu: !action.payload
            }
        }
        case TOGGLE_FORM: {
            return {
                ...state,
                addIncomeForm: !action.payload,
                formRole: action.formType
            }
        }
        case SET_PAGE_NAME: {
            return {
                ...state,
                pageName: action.pageName 
            }
        }
        case TOGGLE_CATEGORY_FORM: {
            return {
                ...state,
                addCategoryForm: !state.addCategoryForm,
                formRole: action.formType
            }
        }
        default: {
            return state
        }
    }
}