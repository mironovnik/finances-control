export type formRoleType = "income" | "outcome"

export const TOGGLE_OVERLAY: string = `TOGGLE_OVERLAY`;

export const TOGGLE_MENU: string = `TOGGLE_MENU`;

export const SET_PAGE_NAME: string = `SET_PAGE_NAME`;

export const TOGGLE_FORM: string = `TOGGLE_FORM`;

export const TOGGLE_CATEGORY_FORM: string = `TOGGLE_CATEGORY_FORM`;

export type toggleMenuActionType = {
    type: typeof TOGGLE_MENU,
    payload: boolean
}

export type toggleFormActionType = {
    action: typeof TOGGLE_FORM,
    payload: boolean,
    formType: formRoleType
}

export type closeOverlayActionType = {
    type: typeof TOGGLE_OVERLAY,
    payload: boolean
}

export type setPageActionType = {
    type: typeof SET_PAGE_NAME, 
    pageName: string
}

export type layoutActionType = toggleMenuActionType & toggleFormActionType & closeOverlayActionType & setPageActionType