import { TOGGLE_OVERLAY, TOGGLE_MENU, TOGGLE_FORM, SET_PAGE_NAME, TOGGLE_CATEGORY_FORM, formRoleType } from './layoutActionTypes';

export function toggleOverlay(toggleMode: boolean) {
    return {
        type: TOGGLE_OVERLAY,
        payload: toggleMode
    }
}

export function toggleMenu(toggleMode: boolean) {
    return {
        type: TOGGLE_MENU,
        payload: toggleMode
    }
}

export function toggleIncomeForm(toggleMode: boolean, formType: formRoleType) {
    return {
        type: TOGGLE_FORM,
        payload: toggleMode,
        formType
    }
} 

export function setPageName(pageName: string) {
    return {
        type: SET_PAGE_NAME,
        pageName
    }
}

export function toggleCategoryForm(formRole: formRoleType) {
    return {
        type: TOGGLE_CATEGORY_FORM,
        formType: formRole
    }
}