import { formRoleType } from '../../../components/Income/AddIncomeForm/types';
import { IncomesObj } from '../../../containers/CommonOutIn/types'
import { AppStateType } from '../../reducers/rootReducer'
import { SHOW_MONTH_STAT, CREATE_INCOME, FETCH_STATISTIC_SUCCESS, GET_LOCAL_STATISTIC } from './incomesActionTypes'
import { Dispatch } from 'redux'
import axios from 'axios'

export type statisticType = {
    [key: string]: IncomesObj
}

function saveStaticticInLocalStorage(incomeConfig: IncomesObj, formType: formRoleType) {
    localStorage.setItem(`${formType}_statictic`, JSON.stringify(incomeConfig));
}

async function saveStatisticServer(statistic: statisticType, userId: string) {
    try {
        await axios.post(`https://finance-know.firebaseio.com/users/${userId}.json`, statistic);
    } catch (e) {
        console.log(`saveStatisticServer error=>`, e);
    }
}

export function createIncome(incomeConfig: IncomesObj, formType: formRoleType) {
    return (dispatch: Dispatch, getState: () => AppStateType) => {
        dispatch(addIncomeLocal(incomeConfig, formType));
        const statistic = {
            monthIncomesStatistic: getState().incomes.monthIncomesStatistic,
            monthOutcomesStatistic: getState().incomes.monthOutcomesStatistic
        };
        const userId = localStorage.getItem(`userId`);
        if (userId) {
            saveStatisticServer(statistic, userId);
        } else {
            saveStaticticInLocalStorage(incomeConfig, formType);
        }
    }
}

export function addIncomeLocal(incomeConfig: IncomesObj, formType: formRoleType) {
    const typeOfAdd = formType === `income` ? `monthIncomesStatistic` : `monthOutcomesStatistic`;
    return {
        type: CREATE_INCOME,
        payload: incomeConfig,
        typeOfAdd
    }
}

export function getIncomesStatisticByMonth(monthIndex: number) {
    return (dispatch: Dispatch, getState: () => AppStateType) => {
        const displayedDate = getState().incomes.displayedDate
        if (monthIndex === 12) {
            displayedDate.year++;
            monthIndex = 0;
        }
        if (monthIndex === -1) {
            displayedDate.year--;
            monthIndex = 11;
        }
        displayedDate.monthIndex = monthIndex;
        dispatch({
            type: SHOW_MONTH_STAT,
            payload: {
                displayedDate,
                displayedStatisticIndex: monthIndex + 1 + '' + displayedDate.year
            }
        })
    }
}

export function getAllStatisticFromRemote(userId: string) {
    return async (dispatch: Dispatch) => {
        try {
            const response = await axios.get(`https://finance-know.firebaseio.com/users/${userId}.json`);
            if (response.data) {
                const financeKeys = Object.keys(response.data);
                dispatch(fetchStatisticSuccess(response.data[financeKeys[financeKeys.length - 1]]));
            }
        } catch (e) {
            console.log(`get all statistic error`, e);
        }
    }
}

export function getAllStatisticFromLocal() {
    return {
        type: GET_LOCAL_STATISTIC
    }
}

export function fetchStatisticSuccess(statistic: statisticType) {
    return {
        type: FETCH_STATISTIC_SUCCESS,
        statistic
    }
}
