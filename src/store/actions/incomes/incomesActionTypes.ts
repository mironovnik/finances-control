export const SHOW_MONTH_STAT: string = `SHOW_MONTH_STAT`;

export type IncomeType = {
    date: string,
    category: string,
    comment?: string,
    amount: number
}

export type IncomesObj = {
    [key: string]: Array<IncomeType>
}

export type showMonthStatType = {
    displayedDate: {
        monthIndex: number,
        year: number
    },
    displayedStatisticIndex: string,
    monthStatistic: IncomesObj
}

export const CREATE_INCOME: string = `CREATE_INCOME`;

export const FETCH_STATISTIC_SUCCESS: string = `FETCH_STATISTIC_SUCCESS`;

export const GET_LOCAL_STATISTIC: string = `GET_LOCAL_STATISTIC`;

export type createIncomeActionType = {
    action: typeof CREATE_INCOME,
    payload: IncomesObj,
    typeOfAdd: `monthIncomesStatistic` | `monthOutcomesStatistic`
}

export type showMonthStatActionType = {
    type: typeof SHOW_MONTH_STAT,
    payload: showMonthStatType
}

export type fetchStatSuccessActionType = {
    type: typeof FETCH_STATISTIC_SUCCESS,
    statistic: IncomesObj
}

export type incomesActionType = createIncomeActionType & showMonthStatActionType & fetchStatSuccessActionType