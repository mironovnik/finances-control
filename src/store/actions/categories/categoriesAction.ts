import { keyType, categoriesType } from '../../../components/Categories/types';
import { Dispatch } from 'redux'
import { ADD_CATEGORY } from './categoriesActionTypes'
import { formRoleType } from '../../../components/Income/AddIncomeForm/types'
import axios from 'axios'
import { AppStateType } from '../../reducers/rootReducer'

const saveCategoryServer = async (userId: string, categories: categoriesType) => {
    try {
        await axios.post(`https://finance-know.firebaseio.com/users/${userId}/categories.json`, categories);
    } catch (e) {
        console.log(`saveCategoryServer error=>`, e);
    }
}

const saveCategoryLocal = (categories: categoriesType) => {
    localStorage.setItem(`categories`, JSON.stringify(categories));
}

export function addCategory(categoryName: string, formType: formRoleType) {
    return (dispatch: Dispatch, getState: () => AppStateType) => {
        const categoryType: keyType = formType === `income` ? `incomes` : `outcomes`;
        const category = { text: categoryName, defaultValue: false, value: categoryName };
        const categories = getState().categories;
        categories[categoryType].push(category);

        dispatch(addCategoryLocal(categories));
        const userId = localStorage.getItem(`userId`);
        if (userId) {
            saveCategoryServer(userId, categories);
        } else {
            saveCategoryLocal(categories);
        }
    }
}

export function addCategoryLocal(categories: categoriesType) {
    return {
        type: ADD_CATEGORY,
        categories
    }
}

export function getCategoriesFromRemote(userId: string) {
    return async (dispatch: Dispatch) => {
        try {
            const response = await axios.get(`https://finance-know.firebaseio.com/users/${userId}/categories.json`);
            const categoriesKeys = Object.keys(response.data);
            dispatch(addCategoryLocal(response.data[categoriesKeys[categoriesKeys.length - 1]]));
        } catch (e) {
            console.log(`getCategoriesFromRemote error`);
        }
    }
}

export function getCategoriesFromLocal() {
    const categories = localStorage.getItem(`categories`) ? JSON.parse(localStorage.getItem(`categories`) as string) : {}
    return {
        type: ADD_CATEGORY,
        categories
    }
}