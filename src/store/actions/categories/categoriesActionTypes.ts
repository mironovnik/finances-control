export const ADD_CATEGORY: string = `ADD_CATEGORY`;

export type optionType = {
    text: string,
    defaultValue: boolean,
    value: string
}

export type keyType = "incomes" | "outcomes"

export type categoriesType = {
    [key in keyType]: Array<optionType>
}

export type categoriesActionType = {
    type: typeof ADD_CATEGORY,
    categories: categoriesType
}