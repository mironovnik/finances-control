import { AUTH_SUCCESS, AUTH_START, AUTH_FAIL, AUTH_LOGOUT } from './authActionTypes';
import { Dispatch, AnyAction } from 'redux'
import axios from 'axios'
import { ThunkDispatch } from 'redux-thunk';
import { AppStateType } from '../../reducers/rootReducer';

export function auth(email: string, password: string, isLogin: boolean) {
    return async (dispatch: Dispatch & ThunkDispatch<AppStateType, void, AnyAction>) => {
        const loaderName = isLogin ? `loginLoader` : `registerLoader`;
        dispatch(authStart(loaderName));

        const authData = {
            email,
            password,
            returnSecureToken: true
        }

        let url = `https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=AIzaSyAbqcKpkW0RjbXcKzr6txEkaUlLiBp0F4M`;
        if (isLogin) {
            url = `https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=AIzaSyAbqcKpkW0RjbXcKzr6txEkaUlLiBp0F4M`;
        }

        try {
            const response = await axios.post(url, authData);
            const data = response.data;
            const expirationTime = new Date().getTime() + data.expiresIn * 1000;

            localStorage.setItem(`userId`, data.localId);
            localStorage.setItem(`token`, data.idToken);
            localStorage.setItem(`expirationTime`, expirationTime as unknown as string);

            dispatch(authSuccess(data.idToken, loaderName));
            dispatch(autoLogout(expirationTime));
        } catch (error) {
            dispatch(authFail(loaderName));
        }
    }
}

export function authStart(loader: string) {
    return {
        type: AUTH_START,
        loader
    }
}

export function authFail(loader: string) {
    return {
        type: AUTH_FAIL,
        loader
    }
}

export function authSuccess(token: string, loader?: string) {
    return {
        type: AUTH_SUCCESS,
        token,
        loader
    }
}

export function authLogout() {
    localStorage.removeItem(`userId`);
    localStorage.removeItem(`token`);
    localStorage.removeItem(`expirationTime`);
    return {
        type: AUTH_LOGOUT
    }
}

export function autoLogin() {
    return (dispatch: Dispatch & ThunkDispatch<AppStateType, void, AnyAction>) => {
        const token = localStorage.getItem(`token`);
        if (!token) {
            dispatch(authLogout());
        } else {
            const expirationTime = localStorage.getItem(`expirationTime`) as unknown as number;
            if (expirationTime <= new Date().getTime()) {
                dispatch(authLogout());
            } else {
                dispatch(authSuccess(token));
                dispatch(autoLogout((expirationTime - new Date().getTime())))
            }
        }
    }
}

export function autoLogout(time: number) {
    return (dispatch: ThunkDispatch<AppStateType, void, AnyAction>) => {
        setTimeout(() => {
            dispatch(authLogout())
        }, time)
    }
}