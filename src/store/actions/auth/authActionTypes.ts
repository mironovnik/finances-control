export const AUTH_SUCCESS: string = `AUTH_SUCCESS`;

export const AUTH_FAIL: string = `AUTH_FAIL`;

export const AUTH_START: string = `AUTH_START`;

export const AUTH_LOGOUT: string = `AUTH_LOGOUT`;

export type authStartType = {
    type: typeof AUTH_START,
    loader: string
}

export type authSuccessActionType = {
    type: typeof AUTH_SUCCESS,
    token: string
}

export type authActionTypes = authSuccessActionType & authStartType